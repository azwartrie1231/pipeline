const products = [
  {
    id: 1,
    name: "Product 1",
    price: 10000,
    stock: 10,
  },
  {
    id: 2,
    name: "Product 2",
    price: 10000,
    stock: 10,
  }
]

const checkouts = []

const productController = {
  checkout: (req, res) => {
    const { id, quantity } = req.body;

    const productExist = products.find((product) => product.id == id)

    if(productExist) {
      productExist.stock -= quantity;
      checkouts.push({
        id,
        quantity
      })
      return res.status(200).send("Produk berhasil di checkout")
    }

    return res.status(400).send("Produk tidak ditemukan")
  }
}

module.exports = {
  productController,
  products,
};