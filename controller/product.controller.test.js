const { productController, products } = require("./product.controller")

describe('Product Controller', () => {
  describe('checkout', () => {
    const mockResponse = {}
    mockResponse.status = jest.fn().mockReturnValue(mockResponse)
    mockResponse.send = jest.fn().mockReturnValue(mockResponse)

    const mockProduct = {
      find: products.find = jest.fn(),
    }

    afterEach(() => {
      jest.restoreAllMocks();
    })

    it('should be exist', () => {
      expect(productController.checkout).toBeDefined();
    })

    it('should be error when product not exist', () => {
      //given 
      const req = {
        body: {
          id: 5,
          quantity: 1
        }
      }

      mockProduct.find.mockReturnValue(null)
      
      //when
      productController.checkout(req, mockResponse);
      
      //expectation
      expect(mockResponse.status).toBeCalledWith(400)

    })

    it('should be success checkout product', () => {
      //given 
      const req = {
        body: {
          id: 1,
          quantity: 1
        }
      }

      mockProduct.find.mockReturnValue(products[0])
      
      //when
      productController.checkout(req, mockResponse);
      
      //expectation
      expect(mockResponse.status).toBeCalledWith(200)
      expect(products[0].stock).toEqual(9)
    })

  })
})