const express = require("express");
const api = require("./routes/api.route");

const app = express();
app.use(express.json());
app.use(api);

app.use((req, res) => {
  res.sendStatus(200);
});

app.listen(3000, () => console.log("Server run on port 3000"));
