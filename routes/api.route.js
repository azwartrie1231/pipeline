const api = require('express').Router();
const productRoute = require('./product.route');

api.use('/product', productRoute)

module.exports = api;