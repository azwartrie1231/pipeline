const { productController } = require('../controller/product.controller');

const router = require('express').Router();

router.post("/checkout", productController.checkout);

module.exports = router;